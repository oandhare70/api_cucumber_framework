Feature: trigger Deletebooking API to remove booking data

@DeleteBookingDataWithBookingID
Scenario: trigger API request to delete booking details 
		  Given valid booking ID is provided 
		  When request sent to delete booking endpoint
		  Then validate response status code
		  And validate response body contains created message