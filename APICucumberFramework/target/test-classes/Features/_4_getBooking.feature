Feature: Trigger getbooking API to retrive booking details

@getBookings
Scenario: Trigger API request to retrive booking details
		Given provided valid bookingID
		When request sent to endpoint
		Then validate status code
		And validate rsponse body contains booking details