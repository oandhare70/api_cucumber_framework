endpoint is : https://restful-booker.herokuapp.com/booking/1355



requestbody is : {
    "firstname": "Andrew",
    "lastname": "Tate",
    "totalprice": 290,
    "depositpaid": false,
    "bookingdates": {
        "checkin": "2024-03-20",
        "checkout": "2024-03-28"
    },
    "additionalneeds": "Lunch"
}



Trigger time is : Tue, 19 Mar 2024 08:43:11 GMT



created booking id is: null

response body is : {
    "firstname": "Andrew",
    "lastname": "Tate",
    "totalprice": 290,
    "depositpaid": false,
    "bookingdates": {
        "checkin": "2024-03-20",
        "checkout": "2024-03-28"
    },
    "additionalneeds": "Lunch"
}

