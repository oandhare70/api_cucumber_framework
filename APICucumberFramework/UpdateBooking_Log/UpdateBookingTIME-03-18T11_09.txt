endpoint is : https://restful-booker.herokuapp.com/booking/null



requestbody is : {
    "firstname" : "Rutweek",
    "lastname" : "Salunke",
    "totalprice" : 454,
    "depositpaid" : true,
    "bookingdates" : {
        "checkin" : "2024-03-04",
        "checkout" : "2024-03-23"
    },
    "additionalneeds" : "Spa"
}



Trigger time is : Mon, 18 Mar 2024 05:39:14 GMT



created booking id is: null

response body is : Method Not Allowed

