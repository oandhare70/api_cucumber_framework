endpoint is : https://restful-booker.herokuapp.com/booking/964



requestbody is : {
    "firstname": "Elon",
    "lastname": "Musk",
    "totalprice": 385,
    "depositpaid": true,
    "bookingdates": {
        "checkin": "2024-03-19",
        "checkout": "2024-03-27"
    },
    "additionalneeds": "Dinner"
}



Trigger time is : Tue, 19 Mar 2024 10:31:53 GMT



created booking id is: null

response body is : {
    "firstname": "Elon",
    "lastname": "Musk",
    "totalprice": 385,
    "depositpaid": true,
    "bookingdates": {
        "checkin": "2024-03-19",
        "checkout": "2024-03-27"
    },
    "additionalneeds": "Dinner"
}

