endpoint is : https://restful-booker.herokuapp.com/booking/3552



requestbody is : {
    "firstname" : "Rutweek",
    "lastname" : "Salunke",
    "totalprice" : 454,
    "depositpaid" : true,
    "bookingdates" : {
        "checkin" : "2024-03-04",
        "checkout" : "2024-03-23"
    },
    "additionalneeds" : "Spa"
}



Trigger time is : Fri, 15 Mar 2024 14:27:49 GMT



created booking id is: null

response body is : {
    "firstname": "Rutweek",
    "lastname": "Salunke",
    "totalprice": 454,
    "depositpaid": true,
    "bookingdates": {
        "checkin": "2024-03-04",
        "checkout": "2024-03-23"
    },
    "additionalneeds": "Spa"
}

