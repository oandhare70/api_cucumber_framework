endpoint is : https://restful-booker.herokuapp.com/booking/4728



requestbody is : Request body is not given



Trigger time is : Mon, 18 Mar 2024 10:29:52 GMT



created booking id is: 4728

response body is : {
    "firstname": "Andrew",
    "lastname": "Tate",
    "totalprice": 250,
    "depositpaid": false,
    "bookingdates": {
        "checkin": "2024-03-20",
        "checkout": "2024-03-23"
    },
    "additionalneeds": "Lunch"
}

