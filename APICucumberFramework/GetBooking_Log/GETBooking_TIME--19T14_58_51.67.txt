endpoint is : https://restful-booker.herokuapp.com/booking/4370



requestbody is : Request body is not given



Trigger time is : Tue, 19 Mar 2024 09:28:52 GMT



created booking id is: 4370

response body is : {
    "firstname": "Andrew",
    "lastname": "Tate",
    "totalprice": 250,
    "depositpaid": false,
    "bookingdates": {
        "checkin": "2024-03-20",
        "checkout": "2024-03-23"
    },
    "additionalneeds": "Lunch"
}

