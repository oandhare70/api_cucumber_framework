Feature: Trigger UpdateBooking API to modify the existong record 

@UpdateBookingData
Scenario Outline: Trigger api request with valid updated booking details 
	Given valid request body parameters are provided "<FirstName>", "<LastName>", "<TotalPrice>", "<DepositPaid>", "<CheckInDate>", "<CheckOutDate>", and "<AdditionalNeeds>"
	When the request is sent to update the booking record
	Then validate status code update booking 
	And validate response body contains booking details update booking 
	
	Examples: 
		| FirstName | LastName | TotalPrice | DepositPaid | CheckInDate | CheckOutDate | AdditionalNeeds |
		| Nana      | Patekar  | 211        | true        | 2024-03-17  | 2024-03-25   | Breakfast       |
		| Makrand   | Anaspure | 190        | false       | 2024-03-18  | 2024-03-26   | None            |
		| Elon      | Musk     | 385        | true        | 2024-03-19  | 2024-03-27   | Dinner          |
		| Andrew    | Tate     | 290        | false       | 2024-03-20  | 2024-03-28   | Lunch           |