Feature: Trigger the CreateBooking API to make a new booking 

@CreateBooking
Scenario Outline: Trigger the API request with valid booking details 
	Given required booking details are provided with "<FirstName>", "<LastName>", "<TotalPrice>", "<DepositPaid>", "<CheckInDate>", "<CheckOutDate>", and "<AdditionalNeeds>" 
	When request sent to endpoint with payload 
	Then validate the statuscode 
	And validate response body contains booking api parameters 
	
	Examples: 
		| FirstName | LastName | TotalPrice | DepositPaid | CheckInDate | CheckOutDate | AdditionalNeeds |
		| Nana      | Patekar  | 200        | true        | 2024-03-17  | 2024-03-20   | Breakfast       |
		| Makrand   | Anaspure | 150        | false       | 2024-03-18  | 2024-03-21   | None            |
		| Elon      | Musk     | 300        | true        | 2024-03-19  | 2024-03-22   | Dinner          |
		| Andrew    | Tate     | 250        | false       | 2024-03-20  | 2024-03-23   | Lunch           |