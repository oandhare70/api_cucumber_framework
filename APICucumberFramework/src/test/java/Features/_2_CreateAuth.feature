Feature: Trigger the API to generate Basic Auth based  Authentication token and validate whether token gets created or not

@CreateAuthToken
Scenario: Trigger API request with valid request body parameters
		  Given valid request body parameters and log directory, evidencefilewriter,endpoint are provided
		  When send request with payloads to endpoint
		  Then validating the status code
          And validate the response body contains authentication token