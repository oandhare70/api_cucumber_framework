package CommonMethods;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

public class ExtentListenerClass implements ITestListener {

    ExtentReports extentReport;
    ExtentTest test;

    
    public void reportConfigurations() {
        ExtentSparkReporter sparkReporter = new ExtentSparkReporter(".\\extent-report\\report.html");
        extentReport = new ExtentReports();
        extentReport.attachReporter(sparkReporter);

        // Adding system/environment information to reports
        extentReport.setSystemInfo("os", "Windows 11");
        extentReport.setSystemInfo("user", "Onkar");

        // Configurations for changing look & feel of report
        sparkReporter.config().setDocumentTitle("RestAssured Extent Listener Report");
        sparkReporter.config().setReportName("This is my First Extent-Report");
        sparkReporter.config().setTheme(Theme.DARK);
    } 
    
    
    public void onStart(ITestContext context) {
        reportConfigurations();
        System.out.println("Test suite started: " + context.getName());
    }

    
    public void onFinish(ITestContext context) {
        System.out.println("Test suite finished: " + context.getName());
        extentReport.flush();
    }

    
    public void onTestFailure(ITestResult result) {
        System.out.println("Test method failed: " + result.getName());
        test = extentReport.createTest(result.getName());
        test.log(Status.FAIL,
                MarkupHelper.createLabel("Name of the failed test case is: " + result.getName(), ExtentColor.RED));
    }

   
    public void onTestSkipped(ITestResult result) {
        System.out.println("Test method skipped: " + result.getName());
        test = extentReport.createTest(result.getName());
        test.log(Status.SKIP,
                MarkupHelper.createLabel("Name of the skipped test case is: " + result.getName(), ExtentColor.YELLOW));
    }

    
    public void onTestStart(ITestResult result) {
        System.out.println("Test method started: " + result.getName());
    }

    public void onTestSuccess(ITestResult result) {
        System.out.println("Test method passed: " + result.getName());
        test = extentReport.createTest(result.getName());
        test.log(Status.PASS,
                MarkupHelper.createLabel("Name of the passed test case is: " + result.getName(), ExtentColor.GREEN));
    }

  
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        // Not implemented
    }

    
}
