package CommonMethods;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Utility {

	public static File createLogDirectory(String folderName) {
		String Projectdir = System.getProperty("user.dir");

		File directory = new File(Projectdir + "\\" + folderName);
		System.out.println(directory);

		if (directory.exists()) {
			System.out.println(directory + " already exists\n");
		} else {
			System.out.println(directory + " does not exists hence creating one\n");
			directory.mkdir();
			System.out.println(directory + " is created\n");
		}
		return directory;
	}

	public static void CreateEvidenceFile(File filelocation, String filename, String endpoint, String reqbody,
			String Header, String resbody, String BookingId) throws IOException {

		File newTextFile = new File(filelocation + "\\" + filename + ".txt");
		System.out.println("file created with " + newTextFile.getName() + "\n\n");

		FileWriter writedata = new FileWriter(newTextFile);
		writedata.write("endpoint is : " + endpoint + "\n\n");
		writedata.write("\n\n" + "requestbody is : " + reqbody + "\n\n");
		writedata.write("\n\n" + "Trigger time is : " + Header + "\n\n");
		writedata.write("\n\n" + "created booking id is: " + BookingId);
		writedata.write("\n\n" + "response body is : " + resbody + "\n\n");

		writedata.close();

	}

	public static String createFileName(String name) {
		LocalDateTime crtime = LocalDateTime.now();
		String currenttime = crtime.toString().substring(5, 16).replaceAll(":", "_");
		String filename = name + "TIME-" + currenttime;
		return filename;
	}

	public static String createFileNameforDataDriven(String name) {
		LocalDateTime crtime = LocalDateTime.now();
		String currenttime = crtime.toString().substring(7, 22).replaceAll(":", "_");
		String filename = name + "_TIME-" + currenttime;
		return filename;
	}

	public static ArrayList<String> ReadExcelData(String SheetName, String TestCase) throws IOException {
		String ProjectDir = System.getProperty("user.dir");
		ArrayList<String> arraydata = new ArrayList<String>();
		FileInputStream fis = new FileInputStream(ProjectDir + "\\Data_Inputs\\Bookings_Data.xlsx");

		XSSFWorkbook wb = new XSSFWorkbook(fis);
		int count = wb.getNumberOfSheets();

		for (int i = 0; i < count; i++) {
			if (wb.getSheetName(i).equals(SheetName)) {
				XSSFSheet datasheet = wb.getSheetAt(i);
				Iterator<Row> rows = datasheet.iterator();
				String testCaseFound = "False";
				while (rows.hasNext()) {
					Row datarow = rows.next();
					String testcase = datarow.getCell(0).getStringCellValue();
					if (testcase.equals(TestCase)) {
						testCaseFound = "True";
						Iterator<Cell> cellvalue = datarow.cellIterator();
						while (cellvalue.hasNext()) {
							Cell cell = cellvalue.next();
							String testdata;
							CellType datatype = cell.getCellType();
							if (datatype == CellType.NUMERIC && DateUtil.isCellDateFormatted(cell)) {
								java.util.Date date = cell.getDateCellValue();
								SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
								testdata = sdf.format(date);

							}
							else if (datatype.toString().equals("NUMERIC")) {
								double intdatatype = cell.getNumericCellValue();
								int intdata = (int) intdatatype;
								testdata = String.valueOf(intdata);
							}

							else if (datatype.toString().equals("BOOLEAN")) {
								boolean booleandata = cell.getBooleanCellValue();
								testdata = String.valueOf(booleandata);
							}
							else {
								testdata = cell.getStringCellValue();
							}

							arraydata.add(testdata);

						}
						break;
					}
				}
				if (testCaseFound.equals("False")) {
					System.out.println(TestCase + " not found at" + wb.getSheetName(i));
				}
				break;
			} else {
				System.out.println(SheetName + " data sheet not found in Bookings_Data.xlsx");
			}
		}
		wb.close();
//		System.out.println(arraydata);
		return arraydata;
	}

}
