package CommonMethods;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class RetryAnalyzer implements IRetryAnalyzer {

	
	private int start =0;
	private int end = 4;
	@Override
	public boolean retry(ITestResult result) {
		if(start<end) {
			String testcasename = result.getName();
			System.out.println("Status code is not  found in correct equation"+start+" hence retrying for"+(start+1));
			start++;
			return true;
		}
		return false;
	}

}
