package CommonMethods;


import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class API_Trigger {
	
	public static Response ApiCheck(String headername,String headervalue, String endpoint ) {
		RequestSpecification req_spec = RestAssured.given();
		req_spec.header(headername, headervalue);
		Response response = req_spec.get(endpoint);
		return response;
	}
	public static Response Auth(String headername,String headervalue,String reqbody,String endpoint) {
		RequestSpecification reqspec = RestAssured.given();
//		for(int i=0;i<headername.length;i++) {
//			reqspec.header(headername[i],headervalue[i]);
//		}
		reqspec.header(headername,headervalue);
		reqspec.body(reqbody);
		Response response = reqspec.post(endpoint);
		return response;
	}
	
	public static Response createBooking(String headername, String headervalue, String requestbody, String endpoint) {
		RequestSpecification req_spec = RestAssured.given();
		req_spec.header(headername,headervalue);
		req_spec.body(requestbody);
		
		Response response = req_spec.post(endpoint);
		
		return response;
		}
	
	public static Response getBooking(String headername, String headervalue,String endpoint) {
		RequestSpecification req_spec = RestAssured.given();
		req_spec.header(headername,headervalue);
		Response response = req_spec.get(endpoint);
		return response;
	}
	public static Response UpdateBooking(String[] headername, String[] headervalue,String reqbody,String endpoint) {
		RequestSpecification req_spec = RestAssured.given();
//		set multiple headers
		for (int i=0;i<headername.length;i++) {
			req_spec.header(headername[i],headervalue[i]);
		}
//		req_spec.header(headername,headervalue);
		String username = "admin";
		String Password = "password123";
		req_spec.auth().basic(username, Password);
		req_spec.body(reqbody);
		Response response = req_spec.put(endpoint);
		return response;
	}
	
	public static Response DeleteBooking(String[] headername, String[] headervalue,String reqbody,String endpoint) {
		RequestSpecification req_spec = RestAssured.given();
		for(int i=0;i<headername.length;i++) {
			req_spec.header(headername[i],headervalue[i]);
		}
		Response response = req_spec.delete(endpoint);
		return response;
	}
	
}
