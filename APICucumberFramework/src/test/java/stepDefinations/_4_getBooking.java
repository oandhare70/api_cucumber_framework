package stepDefinations;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import CommonMethods.API_Trigger;
import CommonMethods.Utility;
import Prerequisites.RequestBody;
import stepDefinations._3_CreateBooking;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;

public class _4_getBooking {
	   File folder;
	    String endpoint;
	    Response response;
	    String bookingId;
	@Given("provided valid bookingID")
	public void provided_valid_bookingID() {
		 System.out.println("Before test method called");
	        folder = Utility.createLogDirectory("GetBooking_Log");
	        bookingId = _3_CreateBooking.bookingID; // Fetching the booking ID
	        endpoint = RequestBody.Hostname() + RequestBody.resource_getBookings() + bookingId;
//	    throw new io.cucumber.java.PendingException();
	}
	@When("request sent to endpoint")
	public void request_sent_to_endpoint() throws IOException {
		response = API_Trigger.getBooking(RequestBody.HeaderName(), RequestBody.HeaderValue(), endpoint);
		 Utility.CreateEvidenceFile(folder, Utility.createFileNameforDataDriven("GETBooking"), endpoint,
	                "Request body is not given", response.getHeader("date"), response.getBody().asPrettyString(),
	                bookingId);
//	    throw new io.cucumber.java.PendingException();
	}
	@Then("validate status code")
	public void validate_status_code() {
		Assert.assertEquals(response.statusCode(), 200);
//	    throw new io.cucumber.java.PendingException();
	}
	@Then("validate rsponse body contains booking details")
	public void validate_rsponse_body_contains_booking_details() {
//		Assert.assertEquals(response.statusCode(), 200);
//	    throw new io.cucumber.java.PendingException();
	}
	
}
