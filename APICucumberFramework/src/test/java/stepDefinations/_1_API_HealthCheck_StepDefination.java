package stepDefinations;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import CommonMethods.API_Trigger;
import CommonMethods.Utility;
import Prerequisites.RequestBody;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;

public class _1_API_HealthCheck_StepDefination {
	
	File folder;
	String Endpoint;
	int statuscode;
	Response response;
	
	@Given("no body is given as we are checking if the api is up and running with GET request")
	public void no_body_is_given_as_we_are_checking_if_the_api_is_up_and_running_with_get_request(){
	    folder = Utility.createLogDirectory("ApiHealthCheck_log");
	    Endpoint = RequestBody.Hostname()+RequestBody.resource_APICheck();		
//	    throw new io.cucumber.java.PendingException();
	}
	
	@When("send the request with payload to endpoint")
	public void send_the_request_with_payload_to_endpoint() throws IOException {
		response = API_Trigger.ApiCheck(RequestBody.HeaderName(), RequestBody.HeaderValue(), Endpoint);
		statuscode = response.getStatusCode();
		Utility.CreateEvidenceFile(folder, Utility.createFileName("CheckAPILog"), Endpoint, "request body is not given",
				response.getHeader("date"), response.getBody().asPrettyString(), null);
//	    throw new io.cucumber.java.PendingException();
	}
	@Then("validation of the status code")
	public void validation_of_the_status_code() {
		Assert.assertEquals(statuscode, 201);
//	    throw new io.cucumber.java.PendingException();
	}



}
