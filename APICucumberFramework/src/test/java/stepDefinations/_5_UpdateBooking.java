package stepDefinations;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import CommonMethods.API_Trigger;
import CommonMethods.Utility;
import Prerequisites.RequestBody;
//import TestCases._2_CreateAuth;
//import s._3_CreateBooking;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class _5_UpdateBooking {

	File folder;
	String Endpoint;
	String reqbody;
	Response response;
	int statuscode;
	String bookingId;

	String res_firstname;
	String res_lastname;
	String res_totalprice;
	String res_depositpaid;
	String res_checkindate;
	String res_checkoutdate;
	String res_additionalneeds;
	
	

	public String UpdateBookingRequestBody(String firstName, String lastName, String totalPrice, String depositPaid,
			String checkInDate, String checkOutDate, String additionalNeeds) {
		// Generate the request body dynamically based on the provided data
		String requestBody = "{\n" + "    \"firstname\": \"" + firstName + "\",\n" + "    \"lastname\": \"" + lastName
				+ "\",\n" + "    \"totalprice\": " + totalPrice + ",\n" + "    \"depositpaid\": " + depositPaid + ",\n"
				+ "    \"bookingdates\": {\n" + "        \"checkin\": \"" + checkInDate + "\",\n"
				+ "        \"checkout\": \"" + checkOutDate + "\"\n" + "    },\n" + "    \"additionalneeds\": \""
				+ additionalNeeds + "\"\n" + "}";
		return requestBody;
	}

//	@Given("valid request body parameters are provided {string}, {string}, {string}, {string}, {string}, {string}, and {string}")
//	 public void valid_request_body_parameters_are_provided(String firstName, String lastName, String totalPrice, String depositPaid, String checkInDate, String checkOutDate, String additionalNeeds) throws IOException {
//		folder = Utility.createLogDirectory("UpdateBooking_Log");
//		reqbody = UpdateBookingRequestBody(firstName, lastName, totalPrice, depositPaid, checkInDate, checkOutDate, additionalNeeds);
//		bookingId = _3_CreateBooking.bookingID;
//		Endpoint = RequestBody.Hostname() + RequestBody.resource_updateBooking() + bookingId;
//
//	}
	@Given("valid request body parameters are provided {string}, {string}, {string}, {string}, {string}, {string}, and {string}")
	public void valid_request_body_parameters_are_provided(String firstName, String lastName, String totalPrice, String depositPaid, String checkInDate, String checkOutDate, String additionalNeeds) throws IOException {
	    folder = Utility.createLogDirectory("UpdateBooking_Log");
	    reqbody = UpdateBookingRequestBody(firstName, lastName, totalPrice, depositPaid, checkInDate, checkOutDate, additionalNeeds);
	    bookingId = _3_CreateBooking.bookingID;
	    Endpoint = RequestBody.Hostname() + RequestBody.resource_updateBooking() + bookingId;
	}



	@When("the request is sent to update the booking record")
	public void the_request_is_sent_to_update_the_record() throws IOException {
		response = API_Trigger.UpdateBooking(new String[] { "Content-Type", "Cookie" },
				new String[] { "application/json", "token=" + _2_CreateAuth_StepDefination.token }, reqbody, Endpoint);

		Utility.CreateEvidenceFile(folder, Utility.createFileNameforDataDriven("UpdateBooking"), Endpoint, reqbody,
				response.getHeader("date"), response.getBody().asPrettyString(), null);
		statuscode = response.statusCode();

		ResponseBody resbody = response.getBody();

		res_firstname = resbody.jsonPath().getString("firstname");
		res_lastname = resbody.jsonPath().getString("lastname");
		res_totalprice = resbody.jsonPath().getString("totalprice");
		res_depositpaid = resbody.jsonPath().getString("depositpaid");
		res_checkindate = resbody.jsonPath().getString("bookingdates.checkin");
		res_checkoutdate = resbody.jsonPath().getString("bookingdates.checkout");

		res_additionalneeds = resbody.jsonPath().getString("additionalneeds");

	}

	@Then("validate status code update booking")
	public void validate_status_code_update_booking() {
		Assert.assertEquals(statuscode, 200);

	}

	@Then("validate response body contains booking details update booking")
	public void validate_response_body_contains_booking_details_update_booking() {
		JsonPath jpathreqbody = new JsonPath(reqbody);
		String req_firstname = jpathreqbody.getString("firstname");
//		System.out.println(req_firstname);
		String req_lastname = jpathreqbody.getString("lastname");
		String req_totalprice = jpathreqbody.getString("totalprice");
		String req_depositpaid = jpathreqbody.getString("depositpaid");
		String req_checkindate = jpathreqbody.getString("bookingdates.checkin");
		String req_checkoutdate = jpathreqbody.getString("bookingdates.checkout");
		String req_additionalneeds = jpathreqbody.getString("additionalneeds");
		// validate response with testngs assert

//		Assert.assertNotNull(CreateBooking.bookingId);
		Assert.assertEquals(statuscode, 200);
		Assert.assertEquals(res_firstname, req_firstname);
		Assert.assertEquals(res_lastname, req_lastname);
		Assert.assertEquals(res_totalprice, req_totalprice);
		Assert.assertEquals(res_depositpaid, req_depositpaid);
		Assert.assertEquals(res_checkindate, req_checkindate);
		Assert.assertEquals(res_checkoutdate, req_checkoutdate);
		Assert.assertEquals(res_additionalneeds, req_additionalneeds);

	}
}
