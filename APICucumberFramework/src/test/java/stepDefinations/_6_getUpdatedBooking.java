package stepDefinations;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import CommonMethods.API_Trigger;
import CommonMethods.Utility;
import Prerequisites.RequestBody;
//import TestCases._3_CreateBooking;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;

public class _6_getUpdatedBooking {

	File folder;
	String endpoint;
	int Statuscode;
	String bookingId;
	Response response;
	
	@Given("valid bookingID")
	public void valid_bookingID() {
		folder = Utility.createLogDirectory("GetUpdatedBooking_Log");
		bookingId = _3_CreateBooking.bookingID;
		endpoint = RequestBody.Hostname() + RequestBody.resource_getBookings() + bookingId;
		

	}
	@When("send request to endpoint")
	public void send_request_to_endpoint() throws IOException {
		response = API_Trigger.getBooking(RequestBody.HeaderName(), RequestBody.HeaderValue(), endpoint);
		Statuscode = response.statusCode();
		Utility.CreateEvidenceFile(folder, Utility.createFileNameforDataDriven("GETUpdatedBooking"), endpoint,
				"Request body is not given", response.getHeader("date"), response.getBody().asPrettyString(),
				bookingId);
	}
	@Then("validate status code updated booking")
	public void validate_status_code_updated_booking() {
		Assert.assertEquals(response.statusCode(), 200);
//	    throw new io.cucumber.java.PendingException();
	}
	@Then("validate uipdated response body parameters")
	public void validate_uipdated_response_body_parameters() {
//		Assert.assertEquals(response.statusCode(), 200);
//	    throw new io.cucumber.java.PendingException();
	}
}
