package stepDefinations;

import java.io.File;
import java.io.IOException;

import org.junit.Assert;

import CommonMethods.API_Trigger;
import CommonMethods.Utility;
import Prerequisites.RequestBody;
//import TestCases._2_CreateAuth;
//import TestCases._3_CreateBooking;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;

public class _7_DeleteBooking {

	File folder;
	String endpoint;
	int Statuscode;
	Response response;
	String bookingId;

	@Given("valid booking ID is provided")
	public void valid_booking_id_is_provided() {
		folder = Utility.createLogDirectory("DeleteBooking_Log");
		bookingId = _3_CreateBooking.bookingID;
		endpoint = RequestBody.Hostname() + RequestBody.resource_deleteBooking() + bookingId;
	}

	@When("request sent to delete booking endpoint")
	public void request_sent_to_delete_booking_endpoint() throws IOException {

		response = API_Trigger.DeleteBooking(new String[] { "Content-Type", "Cookie" },
				new String[] { "application/json", "token=" + _2_CreateAuth_StepDefination.token },
				"no need of body have booking id", endpoint);

		Statuscode = response.getStatusCode();
		Utility.CreateEvidenceFile(folder, Utility.createFileNameforDataDriven("DeleteBooking"), endpoint,
				"no need to have booking id", response.getHeader("date"), response.getBody().asPrettyString(), null);

	}

	@Then("validate response status code")
	public void validate_response_status_code() {
		Assert.assertEquals(response.statusCode(), 201);
	}

	@Then("validate response body contains created message")
	public void validate_response_body_contains_created_message() {
		if (response.getStatusCode() == 201) {
			System.out.println("\nBooking with ID " + bookingId + " is deleted\n");
		} else {
			System.out.println("Booking is not being able to delete");
		}
	}

}
