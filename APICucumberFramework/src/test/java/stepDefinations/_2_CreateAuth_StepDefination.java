package stepDefinations;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import CommonMethods.API_Trigger;
import CommonMethods.Utility;
import Prerequisites.RequestBody;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;

public class _2_CreateAuth_StepDefination {
	public static String token;
	File folder;
	String Endpoint;
	String reqbody;
	int Statuscode;
	Response response;

	@Given("valid request body parameters and log directory, evidencefilewriter,endpoint are provided")
	public void valid_request_body_parameters_and_log_directory_evidencefilewriter_endpoint_are_provided() {
		folder = Utility.createLogDirectory("CreateAuth_log");
		Endpoint = RequestBody.Hostname() + RequestBody.resource_Auth();
		reqbody = RequestBody.AuthBody();
//	    throw new io.cucumber.java.PendingException();
	}



	@When("send request with payloads to endpoint")
	public void send_request_with_payloads_to_endpoint() throws IOException {
	    response = API_Trigger.Auth(RequestBody.HeaderName(), RequestBody.HeaderValue(), RequestBody.AuthBody(),
	            Endpoint);
	    Statuscode = response.getStatusCode(); 
	    token = response.jsonPath().getString("token");
	    Utility.CreateEvidenceFile(folder, Utility.createFileName("CreateAuth"), Endpoint, null,
	            response.getHeader("date"), response.getBody().asPrettyString(), null);
	}


	@Then("validating the status code")
	public void validating_the_status_code() {
		Assert.assertEquals(Statuscode, 200);
//    throw new io.cucumber.java.PendingException();
	}

	@Then("validate the response body contains authentication token")
	public void validate_the_response_body_contains_authentication_token() {
		Assert.assertEquals(response.getStatusLine(), "HTTP/1.1 200 OK");
//    throw new io.cucumber.java.PendingException();
	}
}
