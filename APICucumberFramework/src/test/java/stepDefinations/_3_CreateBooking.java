package stepDefinations;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import CommonMethods.API_Trigger;
import CommonMethods.Utility;
import Prerequisites.RequestBody;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class _3_CreateBooking {

	public static String bookingID;
	String reqbody;
	String endpoint;
	File folder;
	Response response;
	int statuscode;
	ResponseBody resbody;

	String res_firstname;
	String res_lastname;
	String res_totalprice;
	String res_depositpaid;
	String res_checkindate;
	String res_checkoutdate;
	String res_additionalneeds;

	public String generateBookingRequestBody(String firstName, String lastName, String totalPrice, String depositPaid,
			String checkInDate, String checkOutDate, String additionalNeeds) {
		// Generate the request body dynamically based on the provided data
		String requestBody = "{\n" + "    \"firstname\": \"" + firstName + "\",\n" + "    \"lastname\": \"" + lastName
				+ "\",\n" + "    \"totalprice\": " + totalPrice + ",\n" + "    \"depositpaid\": " + depositPaid + ",\n"
				+ "    \"bookingdates\": {\n" + "        \"checkin\": \"" + checkInDate + "\",\n"
				+ "        \"checkout\": \"" + checkOutDate + "\"\n" + "    },\n" + "    \"additionalneeds\": \""
				+ additionalNeeds + "\"\n" + "}";
		return requestBody;
	}

	@Before
	public void setup() {
		folder = Utility.createLogDirectory("createBooking_log");
	}

	@Given("required booking details are provided with {string}, {string}, {string}, {string}, {string}, {string}, and {string}")
	public void required_booking_details_are_provided_with(String firstName, String lastName, String totalPrice,String depositPaid, String checkInDate, String checkOutDate, String additionalNeeds) {
		endpoint = RequestBody.Hostname() + RequestBody.resource_createBooking();
		reqbody = generateBookingRequestBody(firstName, lastName, totalPrice, depositPaid, checkInDate, checkOutDate,additionalNeeds);
	}

	@When("request sent to endpoint with payload")
	public void request_sent_to_endpoint_with_payload() throws IOException {
		response = API_Trigger.createBooking(RequestBody.HeaderName(), RequestBody.HeaderValue(), reqbody, endpoint);
		resbody = response.getBody();
		bookingID = resbody.jsonPath().getString("bookingid");
		statuscode = response.getStatusCode();
		Utility.CreateEvidenceFile(folder, Utility.createFileNameforDataDriven("createBookingLog"), endpoint,                                             reqbody,response.getHeader("date"), response.getBody().asPrettyString(), bookingID);

		res_firstname = resbody.jsonPath().getString("booking.firstname");
		res_lastname = resbody.jsonPath().getString("booking.lastname");
		res_totalprice = resbody.jsonPath().getString("booking.totalprice");

		res_depositpaid = resbody.jsonPath().getString("booking.depositpaid");
		res_checkindate = resbody.jsonPath().getString("booking.bookingdates.checkin");
		res_checkoutdate = resbody.jsonPath().getString("booking.bookingdates.checkout");

		res_additionalneeds = resbody.jsonPath().getString("booking.additionalneeds");
	}

	@Then("validate the statuscode")
	public void validate_the_statuscode() {
		Assert.assertEquals(statuscode, 200);
	}

	@Then("validate response body contains booking api parameters")
	public void validate_response_body_contains_booking_api_parameters() {

		JsonPath jpathreqbody = new JsonPath(reqbody);
		String req_firstname = jpathreqbody.getString("firstname");

		String req_lastname = jpathreqbody.getString("lastname");
		String req_totalprice = jpathreqbody.getString("totalprice");

		String req_depositpaid = jpathreqbody.getString("depositpaid");
		String req_checkindate = jpathreqbody.getString("bookingdates");
		String req_additionalneeds = jpathreqbody.getString("additionalneeds");

		Assert.assertEquals(statuscode, 200);
		Assert.assertNotNull(bookingID);
		Assert.assertEquals(res_firstname, req_firstname);
		Assert.assertEquals(res_lastname, req_lastname);
		Assert.assertEquals(res_totalprice, req_totalprice);
		Assert.assertEquals(res_depositpaid, req_depositpaid);
		Assert.assertNotNull(res_checkindate);
		Assert.assertEquals(res_additionalneeds, req_additionalneeds);
	}
	
	@After 
	public void closure() {
		System.out.println("test case exicuted and saved successful response.");
	}

}
