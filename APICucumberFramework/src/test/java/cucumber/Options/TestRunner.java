package cucumber.Options;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/Features", glue = {"stepDefinations" }, tags = "@CreateAuthToken or @CreateBooking or @getBookings or @UpdateBookingData or @DeleteBookingDataWithBookingID")
public class TestRunner {

}
